import React, { useState } from 'react'
import UserTable from './UserTable'
import {PeopleInterface,Person} from './types';
import AddUser from './AddUser';
import Pagination from './Pagination';


export class Main extends React.Component<{},PeopleInterface> {
    constructor(props:any){
        super(props);
    this.state = {
        people: ([{ id: 1, name: 'Tania', username: 'floppydiskette' },
        { id: 2, name: 'Craig', username: 'siliconeidolon' },
        { id: 3, name: 'Ben', username: 'benisphere' },{ id: 4, name: 'Tansdia', username: 'floppydiskette' },
        { id: 5, name: 'Craig', username: 'siliconeidolon' },
        { id: 6, name: 'siliconeidolon', username: 'Tania' },
        { id: 7, name: 'Tania', username: 'siliconeidolon' },
        { id: 8, name: 'siliconeidolon', username: 'Tania' },
        { id: 9, name: 'Ben', username: 'Tania' },{ id: 10, name: 'Tania', username: 'floppydiskette' },
        { id: 11, name: 'Tania', username: 'siliconeidolon' },
        { id: 12, name: 'Ben', username: 'benisphere' }] as Person[]),
        deleteUser:'',
        editUser:'',
        pageOfItems:[]
    }

    this.deleteUser=this.deleteUser.bind(this);
    this.onChangePage=this.onChangePage.bind(this);
    this.editUser=this.editUser.bind(this);
}
deleteUser(id:any)
{
const newTodos = this.state.people.filter(item => item.id !== id);
this.setState({ people: newTodos});
 
}
editUser(id:any)
{
const newTodos = this.state.people.filter(item => item.id !== id);
this.setState({ people: newTodos});
 
}
onChangePage(pageOfItems:any) {
 this.setState({ pageOfItems: pageOfItems });
}
handleClick() {
  // this.setState({
  //   people: [...this.state.people, {
  //     id: 5,
  //     name :"dssd",
  //     username :"dfsdf"
   
  //   }]
  // });
 console.log(this.state);
  }
  onChange = (updatedValue:Person) => {
  
    updatedValue.id = this.state.people[this.state.people.length-1].id+1;
    this.setState({
      people: [...this.state.people, {

        ...updatedValue
     
      }]
    });
   
  };
  
    render() {
      
  return (
    <div>
      <h1>CRUD</h1>
      <div >
        <div>
          <h2>Add user</h2>
          <AddUser onChange={(people:Person) => this.onChange(people)} />
        </div>
        <div >
          
          <h2>View users</h2>
          <Pagination people={this.state.people} deleteUser={this.onChangePage} />
          <UserTable people={this.state.pageOfItems} deleteUser={this.deleteUser} editUser={this.editUser}/>
        </div>
   
      </div>
      
      <button onClick={() => this.handleClick()}>
        Click me
      </button>
    </div>
  )
}
}

export default Main