import React from 'react';
import {pageInterface,Person} from './types';
import { number } from 'prop-types';
interface State{
    pager:any;
    pageSize:number;
}



class Pagination extends React.Component<pageInterface,State> {
    
    constructor(props:pageInterface) {
        super(props);
        this.state = { pager: {},
    pageSize:10,
    };
    }
    componentWillMount() {
        // set page if items array isn't empty
        if (this.props.people && this.props.people.length) {
            this.setPage(this.props.people);
        }
    }
    componentDidUpdate(prevProps:any, prevState:any) {
        // reset page if items array has changed
        if (this.props.people !== prevProps.people) {
            this.setPage(1);
        }
    }
 
    setPage(page:any) {
        var { people } = this.props;
        var { pageSize } = this.state;
        var pager = this.state.pager;
        if (page < 1 || page > pager.totalPages) {
            return;
        }
        pager = this.getPager(people.length, page, pageSize);

       
        var pageOfItems = people.slice(pager.startIndex, pager.endIndex + 1);

    
        this.setState({ pager: pager });

       
        this.props.deleteUser(pageOfItems);
    }
    getPager(totalItems:number, currentPage:number, pageSize:number) {


currentPage = currentPage || 1;


pageSize = pageSize || 10;


var totalPages = Math.ceil(totalItems / pageSize);

var startPage:number, endPage:number;
if (totalPages <= 10) {
  
    startPage = 1;
    endPage = totalPages;
} else {
    
    if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
    } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
    } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
    }
}

var startIndex = (currentPage - 1) * pageSize;
var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);
return {
    totalItems: totalItems,
    currentPage: currentPage,
    pageSize: pageSize,
    totalPages: totalPages,
    startPage: startPage,
    endPage: endPage,
    startIndex: startIndex,
    endIndex: endIndex,
    pages: pages
};
    }
    render() {
        // console.log(this.props.people.length);
        var pager = this.state.pager;

       

        return (
            <ul>
                <li>
                    <a onClick={() => this.setPage(1)}>First</a>
                </li>
                <li>
                    <a onClick={() => this.setPage(pager.currentPage - 1)}>Previous</a>
                </li>
               {pager.pages.map((page:number, index:any) =>
                    <li>
                        <a onClick={() => this.setPage(page)}>{page}</a>
                    </li>
                )}
                <li>
                    <a onClick={() => this.setPage(pager.currentPage + 1)}>Next</a>
                </li>
                <li >
                    <a onClick={() => this.setPage(pager.totalPages)}>Last</a>
                </li>
            </ul>
        );
    }

}

export default Pagination;