import React from 'react'
import {editInterface} from './types';


const UserTable =(props: editInterface)=> (

  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      
      {props.people.length > 0 ? (
        props.people.map(user => (
          <tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.username}</td>
            <td>
              <button onClick={() => props.editUser(user.id)} >Edit</button>
              <button onClick={() => props.deleteUser(user.id)} > Delete</button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No users</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default UserTable