import React, { Component } from 'react';
import {PeopleInterface} from './types';


class UserTable extends React.Component<PeopleInterface>{

    // componentWillReceiveProps = (props :PeopleInterface) =>{
        
    //     // this.props.people  = this.props.people
           
    //    console.log(props);
    // }

    
    render() {
      console.log(this.props.people.length);
      return (
         <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      
      {this.props.people.length > 0 ? (
        this.props.people.map(user => (
          <tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.username}</td>
            <td>
              <button className="button muted-button">Edit</button>
              <button className="button muted-button">Delete</button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No users</td>
        </tr>
      )}
    </tbody>
  </table>
      );
    }
  }

export default UserTable