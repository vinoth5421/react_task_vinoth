export interface Person {
    id: number;
    name: string;
    username: string;

}

export interface PeopleInterface {
    people: Person[];
    deleteUser:any;
    pageOfItems:any;
    editUser:any;
   
}
export interface pageInterface {
    people: Person[];
    deleteUser:any;
    
  }
  export interface editInterface {
    people: Person[];
    deleteUser:any;
    editUser:any;
    
  }
export interface PersonInterface {
    person: Person;
}

